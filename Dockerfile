FROM docker:dind

# Add the proxy's CA cert.
RUN \
  apk update -Uv --no-progress && \
  apk add -uv --no-progress \
    ca-certificates && \
  rm -rf /var/cache/apk/*

COPY certs/ca-proxy-cert.pem /usr/local/share/ca-certificates/ca-proxy-cert.crt
RUN update-ca-certificates

# Add support for git.
RUN \
  apk add -uv --no-progress \
    git \
    openssh-client